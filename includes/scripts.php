<?php
  add_action( 'wp_enqueue_scripts', 'main_scripts' );

  function main_scripts() {
    wp_enqueue_style( 'style-main', get_stylesheet_uri() );
    wp_enqueue_style( 'style-main-bundle',  get_template_directory_uri() .'/dist/css/style.bundle.css' );
    wp_enqueue_script( 'script-main', get_template_directory_uri() . '/dist/js/bundle.js', array(), '1.0.0', true );
  }