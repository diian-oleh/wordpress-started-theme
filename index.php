<?php
  $context = Timber::get_context();
  $context['posts'] = Timber::get_posts(['post_type' => 'post']);

  Timber::render('views/index.twig', $context);
