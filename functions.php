<?php
  require_once "vendor/autoload.php";
  $timber = new \Timber\Timber();
  require_once "includes/scripts.php";
  add_filter('timber/context', 'add_to_context');

  function add_to_context($context)
  {
    // So here you are adding data to Timber's context object, i.e...
    // https://timber.github.io/docs/guides/menus/
    $context['menu'] = 'I am some other typical value set in your functions.php file, unrelated to the menu';

    return $context;
  }
